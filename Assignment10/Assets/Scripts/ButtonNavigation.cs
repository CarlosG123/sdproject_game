﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ButtonNavigation : MonoBehaviour {

	public void levelSelection()
    {
        SceneManager.LoadScene("LevelSelection");
    }

    public void toInstructions()
    {
        SceneManager.LoadScene("Instructions");
    }

    public void toScore()
    {
        SceneManager.LoadScene("Highscore");
    }

    public void toMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void toLevel1()
    {
        SceneManager.LoadScene("Level1");
    }

    public void toLevel2()
    {
        SceneManager.LoadScene("Level2");
    }

    public void toCheatScreen()
    {
        SceneManager.LoadScene("CheatScreen");
    }

    public void toLvl2_CheatScreen()
    {
        SceneManager.LoadScene("Lvl2_CheatScreen");
    }

    public void exitMatch()
    {
        SceneManager.LoadScene("MainMenu");
        SceneController._score = 0;
    }
}
