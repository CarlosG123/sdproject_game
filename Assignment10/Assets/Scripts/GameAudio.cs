﻿using UnityEngine;
using System.Collections;

public class GameAudio : MonoBehaviour
{

    public AudioClip mySound;
    AudioSource a;

    int count = 0;

    public void onAndoff()
    {
        if(count == 0)
        {
            count++;
            a.Pause();
        }

        else if(count == 1)
        {
            count--;
            a.Play();
        }
    }

    // Use this for initialization
    void Start()
    {
        a = gameObject.AddComponent<AudioSource>();
        a.clip = mySound;
        a.loop = true;
        a.volume = 1.80f;
        a.Play();

    }

}
