﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class ShowCards : MonoBehaviour
{
    float time = 5; //Seconds for cards to be shown
    public Renderer rend;

    static string textHolder; //Declares static variable
    string cheat = "abc"; //Inputs values into variables
    string cheat2 = "def";
    float cheatTime = 15;

    public void codeEntry() //This method is called if player selects Level 1
    {
        textHolder = GameObject.Find("CodeEntryField").GetComponent<InputField>().text; //Places inputted text into variable 'textHolder'
        SceneManager.LoadScene("Level1"); //Loads Scene
    }

    public void codeEntryLvl2() //This method is called if player selects Level 2
    {
        textHolder = GameObject.Find("CodeEntryField").GetComponent<InputField>().text;
        SceneManager.LoadScene("Level2");
    }

    IEnumerator Start()
    {
        if (SceneManager.GetActiveScene().name == "Level1" && textHolder == cheat) //If its on the correct scene and the text the same as the one in the 'cheat' variable
        {
            rend = GetComponent<Renderer>();
            rend.enabled = false; //Cards will appear
            yield return new WaitForSeconds(cheatTime);
            rend = GetComponent<Renderer>();
            rend.enabled = true; //Card will be covered
        }

        else if (SceneManager.GetActiveScene().name == "Level1") //If the text isn't the same,
        {
            rend = GetComponent<Renderer>();
            rend.enabled = false; //Cards will appear
            yield return new WaitForSeconds(time);
            rend = GetComponent<Renderer>();
            rend.enabled = true; //Card will be covered
        }

        else if (SceneManager.GetActiveScene().name == "Level2" && textHolder == cheat2)
        {
            rend = GetComponent<Renderer>();
            rend.enabled = false; //Cards will appear
            yield return new WaitForSeconds(cheatTime);
            rend = GetComponent<Renderer>();
            rend.enabled = true; //Card will be covered
        }

        else if (SceneManager.GetActiveScene().name == "Level2")
        {
            rend = GetComponent<Renderer>();
            rend.enabled = false; //Cards will appear
            yield return new WaitForSeconds(time);
            rend = GetComponent<Renderer>();
            rend.enabled = true; //Card will be covered
        }
    }
}