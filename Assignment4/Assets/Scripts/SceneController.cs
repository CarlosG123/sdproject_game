﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

    public const int gridRows = 2; //Value of rows
    public const int gridCols = 7; //Value of coloumns
    public const float offsetX = 3f; //Value of x distance
    public const float offsetY = 4f; //Value of y distance

    [SerializeField]
    private MemoryCard frontCard;

    [SerializeField]
    private Sprite[] cardsList;

    //-----------------------------------------------------------------------------

    void Start()
    {
       Vector3 spawnPosition = frontCard.transform.position; //Saved position of first card into startPos

        if (SceneManager.GetActiveScene().name == "Level1")
        {
            int[] pairs = { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6 }; //Double numbers for each card pair
            pairs = cardsShuffle(pairs);

            for (int i = 0; i < gridCols; i++)
            { //Nested loop to define the row and coloumn of the image
                for (int j = 0; j < gridRows; j++)
                {
                    MemoryCard cardContainer; //Container of the original card or its copies
                    if (i == 0 && j == 0)
                    { //Checks if it chooses the original card which is the first one or clones it into another one
                        cardContainer = frontCard;
                    }

                    else
                    {
                        cardContainer = Instantiate(frontCard) as MemoryCard;
                    }

                    int index = j * gridCols + i;
                    int id = pairs[index];
                    cardContainer.SetCard(id, cardsList[id]);

                    float posX = (offsetX * i) + spawnPosition.x; //The offset holds the distance between one card and the other
                    float posY = -(offsetY * j) + spawnPosition.y;
                    cardContainer.transform.position = new Vector3(posX, posY, spawnPosition.z);

                }
            }
        }
    }

    //-----------------------------------------------------------------------------

    private int[] cardsShuffle(int[] numbers) //The paremeter hold the number of pairs that need to be shuffled
    {
        int[] newArray = numbers.Clone() as int[];
        for (int i = 0; i < newArray.Length; i++)
        {
            int tmp = newArray[i]; //tml is stored with the current index of the array 'newArray'
            int r = Random.Range(i, newArray.Length); //r stores the randomly selected value from i and the length of the array.
            newArray[i] = newArray[r];
            newArray[r] = tmp;
        }
        return newArray;
    }

    //-----------------------------------------------------------------------------

    private MemoryCard firstCard;
    private MemoryCard secondCard;


    public bool canReveal{
        get { return secondCard == null; } //Returns false if the secondCard is clicked
    }

    public void cardShown (MemoryCard card){ //This method keeps track of the clicked cards by filling the clicked image status into the card variables. 2 variables are available
        if (firstCard == null)
        {
            firstCard = card;
        }

        else
        {
            secondCard = card;
            StartCoroutine(CheckMatch());
        }
    }


    //-----------------------------------------------------------------------------

    [SerializeField]
    private TextMesh scoreLabel; //Added another variable in the Inspector for the text

    public static int _score = 0;
    public int totalCards = 0; //Keeps track of the number of pairs done
    private IEnumerator CheckMatch()
    {
        if (firstCard.id == secondCard.id) //If both cards match
        {
            totalCards++; //Variable increments
            _score++; //Score increments
            scoreLabel.text = "Score: " + _score; //Text displayed the new incremented value
        }

        else //If pairs don't match
        {
            yield return new WaitForSeconds(.5f);

            firstCard.hideCards(); //Hides cards by calling the function 'hideCards' in MemoryCard
            secondCard.hideCards();

            _score--; //Decrements score
            scoreLabel.text = "Score: " + _score; //Edits the text showing the score
        }

        firstCard = null; //Variables return to 'no value'
        secondCard = null;

        if(totalCards == 7) //If all pairs are matched
        {
            SceneManager.LoadScene("SignUp"); //Change Scene
            
        }
    }

}