﻿using UnityEngine;
using System.Collections;

public class MemoryCard : MonoBehaviour {

    //----------------------------------------------------------------------------// 1
    [SerializeField]
    private GameObject backDisplay; //This kind of variable appears in the inspector in order to set the cardBack
    
    public void OnMouseDown()
    {
        if (backDisplay.activeSelf == true && controller.canReveal) //If cardBack is visible
        {
            backDisplay.SetActive(false); //Sets cardBack invisible
            controller.cardShown(this); //Calls function 'cardShown' from SceneController
        }
    }

    public void hideCards()
    {
        backDisplay.SetActive(true); //Sets the image place in backDisplay visible
    }

    //----------------------------------------------------------------------------// 2
    [SerializeField]
    private SceneController controller;

    private int _id;
    public int id
    {
        get { return _id; }
    }

    public void SetCard(int id, Sprite image)
    {
        _id = id;
        GetComponent<SpriteRenderer>().sprite = image;

    }

    void Start()
    {
    }
}
