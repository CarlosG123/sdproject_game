﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class ShowCards : MonoBehaviour
{
    float time = 5; //Seconds for cards to be shown
    public Renderer rend;

    IEnumerator Start()
    {
        if (SceneManager.GetActiveScene().name == "Level1" || SceneManager.GetActiveScene().name == "Level2") //If the text isn't the same,
        {
            rend = GetComponent<Renderer>();
            rend.enabled = false; //Cards will appear
            yield return new WaitForSeconds(time);
            rend = GetComponent<Renderer>();
            rend.enabled = true; //Card will be covered
        }
    }
}