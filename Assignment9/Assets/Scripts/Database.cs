﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//sqlite dlls so that I can connect to the database
using Mono.Data.Sqlite;
using System.Data;
using System;
//add the scene management library
using UnityEngine.SceneManagement;

    

public class Database : MonoBehaviour
{
    

    IDbConnection dbconn;

    string sqlQuery;

    // Use this for initialization
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Highscore")
        {
            sqlQuery = "SELECT personname,personsurname,points,personid FROM player ORDER BY points DESC"; ////////////////////////////////////
            getData();
        }

    }

    public void addPlayerInfo()
    {
        //Declared Variables
        string inputname, inputsurname;
        int points = SceneController._score;


        //Filled variables with gathered data
        inputname = GameObject.Find("NameInputField").GetComponent<InputField>().text;
        inputsurname = GameObject.Find("SurnameInputField").GetComponent<InputField>().text;

        //Inserted values from variables into table
        string insertstatement =
            "INSERT INTO player(personname,personsurname, points) VALUES('" +
            inputname + "','"
            + inputsurname + "',"
            + points + ");";

        //write the string in console
        Debug.Log(insertstatement);

        //path to the database
        string conn = "URI=file:" + Application.dataPath + "/playerDetails.s3db";

        dbconn = (IDbConnection)new SqliteConnection(conn);

        dbconn.Open(); //Open connection to the database.

        IDbCommand dbcmd = dbconn.CreateCommand();

        dbcmd.CommandText = insertstatement;

        //run the insert statement
        dbcmd.ExecuteNonQuery();

        Debug.Log("inserted successfully");

        //Loads to another Scene
        SceneManager.LoadScene("Highscore");

    }

    public void getData()
    {
        //we connect to the database here.  The application.datapath is the assets folder
        string conn = "URI=file:" + Application.dataPath + "/playerDetails.s3db"; //Path to database. ////////////////////////////////////

        Debug.Log(conn);
        //an instance reference of the database connection

        //create an instance of the connection
        dbconn = (IDbConnection)new SqliteConnection(conn);

        dbconn.Open(); //Open connection to the database.
                       //create a prepared command
        IDbCommand dbcmd = dbconn.CreateCommand();
        
        //set the command text of the query
        dbcmd.CommandText = sqlQuery;
        //read the values
        IDataReader reader = dbcmd.ExecuteReader();

        string textToShow = "";

        while (reader.Read())
        {
            //write all the names under each other
            textToShow += "NAME: " + reader.GetString(0) + "    "
                + " SURNAME: " + reader.GetString(1) + "    "
                + " POINTS: " + reader.GetInt32(2) + "    "////////////////////////////////////
                + " ID: " + reader.GetInt32(3) + "\n";
        }

        GameObject.Find("Text").GetComponent<Text>().text = textToShow;

        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
    }

    // Update is called once per frame
    void Update()
    {

    }
}

